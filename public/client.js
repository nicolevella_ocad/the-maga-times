// elements for animations/UI
const trump = document.getElementById("trump");
const bg = document.getElementById("bg");
const dateDiv = document.getElementById("date");
const deathsDiv = document.getElementById("deaths");
const priceDiv = document.getElementById("price");
const infoDiv = document.getElementById("info");

// init socket
let socket = io();

//init voice
let voice = new p5.Speech();
voice.onLoad = voiceReady();

// an array for the blood drips class 
let bloodDrips = [];

// boolean for socket connection
let connected = false;

// blank tweet placeholder
let tweet = " ";

// numbers for covid death stats
let clockTimer = 0;
let streamTimer = 0;
let tweetTimer = 0;
let deathsPerSecond = 0;
let totalDeaths = 0;
let deathCount = 0;
let nextDeath = 0;

// when someone connects
socket.on("connect", () => {
  // console.log("connected to server");
  // tell the server we are ready for the stream
  socket.emit("start stream", () => {});
});

// when someone disconnects, log it to the console
socket.on("disconnect", () => {
  //  console.log("disconnected from server");
  connected = false;
});

// functions that receive data from server.js
// we will use some regex here to scrub the data
socket.on("sendTweets", (e) => {
  tweet = e.text;

  // regex matching emojiis and carriage returns
  // From Marc Guiselin's answer on this post
  // https://stackoverflow.com/questions/10992921/how-to-remove-emoji-code-using-javascript
  let emojiis = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
  tweet = tweet.replace(emojiis, "");

  // regex matching carriage returns
  let returns = /([\r\n]+|\.|[\r\n])/g;
  tweet = tweet.replace(returns, "");

  // regex matching links (not perfect but pretty good)
  let links = /(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)/g;
  tweet = tweet.replace(links, "");
  // console.log(tweet);
  connected = true;
});

// receive the total amount of deaths, live # through the twit npm
socket.on("sendDeaths", (e) => {
  totalDeaths = e;
});

// receive yesterdays deaths, / by seconds in a day for deaths per second
// use this number to time the blood drops
socket.on("sendYesterDeaths", (e) => {
  deathsPerSecond = floor(86400 / e);
  nextDeath = deathsPerSecond - 1;
});

// setup the sketch
function setup() {
  // canvas full window size with transparent bg
  createCanvas(windowWidth, windowHeight);
  background(0, 0);

  frameRate(15);

  voice.started(startSpeaking);
  voice.ended(stopSpeaking);

  // draw le cheeto and get btc price
  drawTrump();
  getBTCPrice(6);
}

function draw() {
  // get current BTC price value &
  // update the clock
  
  totalDeaths = totalDeaths.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  deathsDiv.innerHTML =
    "<span style='color: rgba(187,0,0)'>" + totalDeaths + "</span> DEAD";
  infoDiv.innerHTML =
    "Someone in the USA <span style='color:rgb(187,0,0)'>dies every " +
    deathsPerSecond +
    " seconds</span> from COVID-19.<br><span style='color:rgb(187,0,0)'>" +
    deathCount +
    " people have died</span> since you loaded this page.<span class='hideme'><br>Click or tap to enable tweet-to-speech.</span>";

  if (millis() >= 1000 + clockTimer) {
    clockTimer = millis();
    if (nextDeath > 0) {
      nextDeath--;
    } else if (nextDeath == 0) {
      bloodDrips.push(
        new bloodDrip(
          random(50, width - 50),
          random(50, height - 250),
          floor(random(22, 44))
        )
      );
      deathCount++;
      nextDeath = deathsPerSecond;
    }
    updateTime(nextDeath);
  }

  // if theres a socket connetion, pull and place the data
  // and begin the bloop drip animation
  if (connected) {
    // a timer to control speed of tweet stream
    if (millis() >= 250 + streamTimer) {
      streamTimer = millis();
      streamTweets(tweet);
    }

    // a timer to control random reading of tweets
    if (millis() >= random(12500, 30000) + tweetTimer) {
      tweetTimer = millis();
      speakTweet(tweet);
    }

    // let the blood flooowwww
    for (let i = 0; i < bloodDrips.length; i++) {
      bloodDrips[i].move();
      bloodDrips[i].show();
      if (bloodDrips[i].y > height) {
        bloodDrips.splice(i, 1);
      }
    }
  }
}

// function to draw the clown in chief
function drawTrump() {
  let trump = document.querySelector("#trump");
  trump.classList.add("closed");
}

// function to get BTC price using blockchain api endpoint
function getBTCPrice(USDValue) {
  fetch("https://blockchain.info/tobtc?currency=USD&value=" + USDValue).then(
    function (response) {
      response.text().then(function (text) {
        priceDiv.innerHTML = text + " <span style='color:rgb(187,0,0)'>BTC</span>";
      });
    }
  );
}

// function to setup voice object
function voiceReady() {
  //  console.log(voice.listVoices());
  voice.setRate(0.9);
  voice.setPitch(0.65);
  voice.setVoice("Google UK English Male");
}

// function to push tweet stream data to DOM
function streamTweets(tweet) {
  if (tweet != undefined) {
    bg.prepend(tweet);
    bg.innerHTML = bg.innerHTML.substring(0, 17500);
  }
}

// function that triggers when p5.speech begins
function startSpeaking() {
  //  console.log("started speaking");
  // open his mouth
  trump.classList.remove("closed");
  trump.classList.add("open");
}

// function that triggers when p5.speech ends
function stopSpeaking() {
  // console.log("stopped speaking");
  // close his mouth (my fav part)
  trump.classList.remove("open");
  trump.classList.add("closed");
}

// function to speak the tweet and inject into the stream as an H2
function speakTweet(tweet) {
  if (tweet != undefined) {
    let headline = document.createElement("h2");
    headline.innerHTML = tweet;
    bg.prepend(headline);
    voice.speak(tweet);
  }
}

// function to update the time and present it in plain english, not numbers
function updateTime(nextDeath) {
  let dateNames = new Date();

  let dayName = [
    "Sunday",
    "monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  let monthName = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  let y = year();
  let d = day();
  let h = hour();
  let m = minute();
  let s = second();

  if (s < 10) {
    s = "0" + s;
  }

  if (m < 10) {
    m = "0" + m;
  }

  dateDiv.innerHTML =
    dayName[dateNames.getDay()] +
    ", " +
    monthName[dateNames.getMonth()] +
    " " +
    d +
    ", " +
    y +
    "  " +
    " <span style='color:rgb(187,0,0);'> -" +
    nextDeath +
    "</span>";
}

// a class for our blood drips
class bloodDrip {
  constructor(x, y, size) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.color = color(random(127, 200), 0, 0, 33);
  }
  move() {
    this.y += 1;
    if (this.size > 15) this.size *= 0.97;
  }
  show() {
    noStroke();
    fill(this.color);
    ellipse(this.x, this.y, this.size, this.size);
  }
}
